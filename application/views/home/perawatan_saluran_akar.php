<section id="news-detail" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <!-- NEWS THUMB -->
                <div class="news-detail-thumb">
                    <div class="news-image">
                        <img src="<?= base_url('assets/banner/') . $artikel->images; ?>" class="img-responsive" alt="">
                    </div>
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Peralatan Lengkap</h2>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/room1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Ruangan bersih dan nyaman.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/bleach1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Peralatan canggih</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url() ?>">
                                    <img src="<?= base_url('assets/images/koral.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3 class="text-capitalize">Alat memenuhi standar</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2 class="text-capitalize">Pilihan Alat Behel</h2>
                                <h4>Poli Gigi Alkindi menawarkan pilihan jenis behel seperti..</h4>
                            </div>
                        </div>


                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="news-detail.html">
                                    <img src="<?= base_url('assets/images/image.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3>Behel Metal.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <img src="<?= base_url('assets/images/asdf.jpg') ?>" class="img-responsive" alt="">
                                <div class="news-info">
                                    <h3>Behel Ceramic</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="news-detail.html">
                                    <img src="<?= base_url('assets/images/pasti.jpg') ?>" class="img-responsive" alt="">
                                </a>
                                <div class="news-info">
                                    <h3>Behel Damon</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            <!-- SECTION TITLE -->
                            <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                <h2>Prosedur Ortodontik di Poli Gigi Alkindi</h2>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url('') ?>">
                                    <img src="<?= base_url('assets/images/konsul1.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <div class="news-info">
                                    <h5 class="text-justify" style="text-indent: 45px;">Pertama, dokter akan melakukan serangkaian pemeriksaan yang meliputi wawancara, hal yang harus kamu catat adalah lakukan pemasangan kawat gigi di klinik gigi ataupun dokter gigi yang sudah terpercaya dan terjamin kualitasnya. Jangan sampai kamu tergoda untuk sembarang pasang behel, karena dapat merusak gigi kamu nantinya.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            <!-- SECTION TITLE -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url('') ?>">
                                    <img src="<?= base_url('assets/images/pemeriksaan.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <div class="news-info">
                                    <h5 class="text-justify" style="text-indent: 45px;">Sebelum dilakukan perawatan ortodonti, dokter kami akan melakukan pemeriksaan fisik, pemeriksaan gigi, pengambilan cetakan gigi dan bagian dalam mulut secara menyeluruh. Selanjutnya, pengambilan foto intra oral dan profil pasien. Pengambilan foto rontgen panoramik dan cephalometri, serta pemeriksaan lain yang dibutuhkan untuk kasus pasien tersebut.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            <!-- SECTION TITLE -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url('') ?>">
                                    <img src="<?= base_url('assets/images/scaling.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <div class="news-info">
                                    <h5 class="text-justify" style="text-indent: 45px;">Sebelum melakukan pemasangan kawat gigi, kamu harus memastikan bahwa gigi kamu bersih dari karang. Karena itu kamu perlu melakukan pembersihan karang gigi atau yang disebut scaling</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            <!-- SECTION TITLE -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url('') ?>">
                                    <img src="<?= base_url('assets/images/pemeriksaan.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <div class="news-info">
                                    <h5 style="text-indent: 45px;" class="text-justify">Jika kondisi gigi kamu ada yang berlubang ataupun ada yang perlu dicabut, maka dokter terlebih dahulu akan mencabut atau menambal gigi kamu sebelum dilakukan pemasangan behel gigi.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6">
                            <!-- SECTION TITLE -->
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                <a href="<?= base_url('') ?>">
                                    <img src="<?= base_url('assets/images/pemeriksaan.jpg') ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <!-- NEWS THUMB -->
                            <div class="news-thumb wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <div class="news-info">
                                    <h5 style="text-indent: 45px;" class="text-justify">Setelah semua prosedur telah dilakukan, barulah dokter siap dan bisa memasangkan behel gigi sesuai dengan kebutuhan dan pilihan yang diinginkan. Setelah selesai pemasangan behel, kamu harus melakukan kontrol rutin. Agar permasalahan yang timbul saat memakai behel mampu diatasi dengan cepat oleh dokter</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <!-- SECTION TITLE -->
                        <br>
                        <div class="section-title wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                            <h5 class="text-justify">Lama perawatan ortodontik tergantung kepada jenis, tingkat keparahan kasus, dan respon gigi atau tubuh pasien terhadap perawatan tersebut. Selama perawatan ortodonti, pasien dianjurkan untuk kontrol rutin dan mematuhi instruksi dari dokter gigi yang merawat, agar tercapai hasil perawatan yang optimal.</h5=>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-5">
                    <div class="news-sidebar">
                        <div class="news-author">
                            <h4>Tentang Poli Gigi Alkindi</h4>
                            <p class="text-justify" style="text-indent: 45px;">Memilih perawatan gigi di Poli Gigi Alkindi, bisa dapat banyak keuntungan, lho! Mulai dari penanganan aman dan nyaman, hingga menikmati harga promo yang gila-gilaan. Selain itu, kamu juga bisa menikmati segala perawatan gigi dengan harga yang terjangkau.</p>
                        </div>

                        <div class="recent-post">
                            <h4>Layanan Kami Lainnya</h4>
                            <?php foreach ($post as $p) : ?>
                                <div class="media">
                                    <div class="media">
                                        <div class="media-object pull-left">

                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="<?= base_url('') . $p->url . '/' . $p->slug ?>" class="text-primary"><i class="fas fa-arrow-right"></i> <?= $p->title ?></a></h4>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="news-categories">
                            <h4>Categories</h4>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Dental</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Cardiology</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Health</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Consultant</a></li>
                        </div>

                        <div class="news-ads sidebar-ads">
                            <h4></h4>
                        </div>


                    </div>
                </div>

            </div>
        </div>
</section>